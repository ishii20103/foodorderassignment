import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import java.io.IOException;

public class FoodForm {
    private JPanel root;
    private JLabel topLabel;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextPane orderedDisplay;
    private JLabel topOrder;
    private JLabel totalText;
    private JButton payButton;
    private JLabel totalDisplay;
    private JButton button7;
    private JButton button8;
    private JButton button9;

    public FoodForm() {
        int wAndH =250;
        int[] total =new int[1];
        total[0] = 0;
        setTotal(0,total);

        Foods[] foodData =new Foods[9];
        foodData[0] = new Foods(1001,"apple","/image/ringo.png");
        foodData[1] = new Foods(2010,"rice ball","/image/onigiri.png");
        foodData[2] = new Foods(3100,"bread","/image/pan.png");
        foodData[3] = new Foods(4004,"mystery meat","/image/niku.png");
        foodData[4] = new Foods(5040,"curry and rice","/image/curry.png");
        foodData[5] = new Foods(6400,"rice","/image/kome.png");
        foodData[6] = new Foods(10400,"grilled fish","/image/yakizakana.png");
        foodData[7] = new Foods(40400,"crab","/image/kani.png");
        foodData[8] = new Foods(100000,"water","/image/water.png");

        orderedDisplay.setText("");
        orderedDisplay.setEditable(false);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pushFoodButton(foodData[0],total);
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pushFoodButton(foodData[1],total);
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pushFoodButton(foodData[2],total);
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pushFoodButton(foodData[3],total);
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pushFoodButton(foodData[4],total);
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pushFoodButton(foodData[5],total);
            }
        });
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pushFoodButton(foodData[6],total);
            }
        });
        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pushFoodButton(foodData[7],total);
            }
        });
        button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pushFoodButton(foodData[8],total);
            }
        });

        setIconSize(button1,foodData[0].imageFilePath,wAndH,wAndH);
        setIconSize(button2,foodData[1].imageFilePath,wAndH,wAndH);
        setIconSize(button3,foodData[2].imageFilePath,wAndH,wAndH);
        setIconSize(button4,foodData[3].imageFilePath,wAndH,wAndH);
        setIconSize(button5,foodData[4].imageFilePath,wAndH,wAndH);
        setIconSize(button6,foodData[5].imageFilePath,wAndH,wAndH);
        setIconSize(button7,foodData[6].imageFilePath,wAndH,wAndH);
        setIconSize(button8,foodData[7].imageFilePath,wAndH,wAndH);
        setIconSize(button9,foodData[8].imageFilePath,wAndH,wAndH);


        payButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(total[0]!=0) {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to checkout?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.PLAIN_MESSAGE);
                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null,
                                "Thank you.The total price is " + total[0] + " yen.",
                                "CHECK OUT",
                                JOptionPane.PLAIN_MESSAGE);
                        orderedDisplay.setText("");
                        total[0] = 0;
                        setTotal(0, total);
                    }
                }else{
                    JOptionPane.showMessageDialog(null,
                            "Please order something.",
                            "NO ORDER",
                            JOptionPane.PLAIN_MESSAGE);
                }
            }
        });
    }

    void setIconSize(JButton button,String filePath,int width,int height){
        try {
            BufferedImage originalImage = ImageIO.read( this.getClass().getResource(filePath));
            BufferedImage sizedImage = new BufferedImage(width,height,BufferedImage.TYPE_3BYTE_BGR);
            sizedImage.createGraphics().drawImage(originalImage.getScaledInstance(width,height, Image.SCALE_AREA_AVERAGING),
                    0,0,width,height,null);
            button.setIcon(new ImageIcon(sizedImage));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    void setTotal(int add,int[] total){
        total[0] += add;
        totalDisplay.setText(total[0]+" yen");
    }
    void pushFoodButton(Foods food,int[] total){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Do you want to order "+food.foodName+"?\nIt costs "+food.value+" yen.",
                "ORDER CONFIRMATION",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.PLAIN_MESSAGE);
        if(confirmation==0){
            setOrdered(food);
            setTotal(food.value,total);
            JOptionPane.showMessageDialog(null,
                    food.foodName+" ordered.",
                    "CONFIRMED",
                    JOptionPane.PLAIN_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null,
                    "Canceled the order.",
                    "CANCEL",
                    JOptionPane.PLAIN_MESSAGE);
        }
    }
    void setOrdered(Foods food){
        String currentText =orderedDisplay.getText();
        orderedDisplay.setText(currentText+food.foodName+" :    "+food.value+" yen"+"\n");
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodForm");
        frame.setContentPane(new FoodForm().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

class Foods {
    int value;
    String foodName;
    String imageFilePath;

    Foods(int value,String foodName,String imageFilePath){
        this.value=value;
        this.foodName=foodName;
        this.imageFilePath=imageFilePath;
    }
}


